# Node-Server-Example

This is a simple nodejs app starts on port 3000 using express framework.
## Installation
```bash
 npm install
```
## Start application
```bash
 npm start or node app/server.js
```
## To build the image from Dockerfile
```bash
 docker build -t node-server:latest .
```
## To start the built docker image
```bash
  docker run -p 3000:3000 node-server:latest
```
## Curl example
```bash
  curl --location --request GET 'http://localhost:3000/ping'
```