var express = require('express');
const pino = require('pino');

var app = express();

//pino constructor takes config object as param
const logger = pino({level: 'info'});

logger.info('Hello world');
logger.info('log entry for testing for remote and locally');

app.get('/ping',function (req,res){
    logger.info('pong');
    res.send('pong');
});

//run server on port 3000
app.listen(3000, function (){
    logger.info('app listening on port 3000');
});
